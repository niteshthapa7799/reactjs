import React from 'react';

function CreateAttachments({title, images}){
    return(
        <div className = "attachments">
            <p className = "details-title">{title} Details:</p>
                <div className = "attachment-flex">
                    <p id = "attachments">Attachments: </p>
                        {images.map((values)=>(
                            <p className = "image-name">{values}</p>
                        ))}
                </div>
        </div>
    )
}

export default CreateAttachments;