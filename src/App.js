import React from 'react';
import './App.css';
import CreateButton from './button';
import CreateAttachments from './attachments';
import Remarks from './remarks';
import Status from './status';
function App() {
const getValues = [
  {key : "created at",value : '2020-06-21'},
  {key : "submission date",value : '2020-06-21'},
  {key : "created by",value : 'Nitesh Thapa'},
  {key : "subject",value : 'Mathematics'}
];
const homework = {
  title:"social ko homework"
}
let images = ['homeworkpage.jpg','homeworkpage1.jpg','homeworkpage2.jpg'];
  return (
    <div className = "container">
      <div className = "overlay"></div>
        <div className = "sub-container">
            <h2 className = "title">{homework.title}</h2>
              <div className = "tab-container">
                {getValues.map((values) => (
                    <CreateButton content = {values.key} data = {values.value}/>
                ))} 
              </div>
                    <CreateAttachments title = {homework.title} images = {images} />
                    <Remarks />
                    <Status />
              <p id = "submit">SUBMIT</p>
              <p id = "cancel">CANCEL</p>
        </div>
    </div>
  );
}

export default App;
