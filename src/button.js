import React from "react";

function CreateButton(props) {
    
    return (
        <div className = "button">
            <p>{props.content} : {props.data}</p>
        </div>
    )
}
export default CreateButton;